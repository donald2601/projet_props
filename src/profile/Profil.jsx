import './Profil.css';
import image from './1.png';

const Profil = (props) => {
    const handelName=(name)=> alert(`Bonjour ${name}`);
    return (
        
        <div className='body'>
            <div className='profil'>
                <div className='infoprofil'>
                    <div className='imageprofil' onClick={handelName(props.fullName)}>
                        {props.children}
                    </div>
                    <div className='nomprofil'>
                        <h3> {props.fullName} </h3>
                    </div>
                </div>
                <div className='autres'>
                    <div className='bio'>
                        <p style={{textAlign:"start",paddingLeft:'10px'}}> {props.bio}</p>
                    </div>
                    <div className='profession'>
                    <p> <h1> Profession </h1>{props.profession} </p>
                    </div>
                    <div>
                        <span><i class="fa-solid fa-right-from-bracket"></i>log out</span>
                        <span><i class="fa-solid fa-house-user"></i> Home</span>
                        <span><i class="fa-solid fa-lock"></i>Password</span>
                    </div>
                </div>
            </div>
        </div>
    );
  
}


export default Profil;
